package com.example.web.second.service;

import com.example.dubbo.test.InvokeService;
import org.springframework.stereotype.Service;

/**
 * author chenshun00@gmail.com
 * 2020/3/5 10:23 上午
 */
@Service("invokeService")
public class InvokeServiceImpl2 implements InvokeService {
    public String test(String test) {
        return "InvokeServiceImpl2";
    }
}
