package com.example.web.second.view;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * author chenshun00@gmail.com
 * 2020/3/5 12:22 下午
 */
@Controller
public class VV implements InitializingBean {

    @GetMapping("vv")
    @ResponseBody
    public String vv() {
        return "!";
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        throw new IllegalArgumentException("xx");
    }
}
