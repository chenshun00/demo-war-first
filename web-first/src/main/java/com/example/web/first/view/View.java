package com.example.web.first.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * author chenshun00@gmail.com
 * 2020/3/5 11:15 上午
 */
@Controller
public class View {

    @ResponseBody
    @GetMapping
    public Object handle() {
        return "1";
    }

}
