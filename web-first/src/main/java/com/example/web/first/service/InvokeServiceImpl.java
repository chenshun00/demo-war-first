package com.example.web.first.service;

import com.example.dubbo.test.InvokeService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.Arrays;

/**
 * author chenshun00@gmail.com
 * 2020/3/5 10:22 上午
 */
@Service("invokeService")
public class InvokeServiceImpl implements InvokeService, InitializingBean {

    private final Environment environment;

    public InvokeServiceImpl(Environment environment) {
        this.environment = environment;
    }

    public String test(String test) {
        return "123";
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        final String[] activeProfiles = environment.getActiveProfiles();
        System.out.println("xx=>" + Arrays.toString(activeProfiles));
    }
}
