package com.example.web.test.view.service;

import com.example.dubbo.test.InvokeService;

import javax.annotation.PostConstruct;

/**
 * author chenshun00@gmail.com
 * 2020/3/5 10:13 上午
 */
public class InnerService {

    private String name;

    private InvokeService invokeService;

    @PostConstruct
    public void init() {
        name = invokeService.test("1123");
    }

    public String handle() {
        return invokeService.test("1232");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        System.out.println("name===>" + this.name);
        this.name = invokeService.test("123");
        System.out.println("name===>" + this.name);
    }

    public InvokeService getInvokeService() {
        return invokeService;
    }

    public void setInvokeService(InvokeService invokeService) {
        this.invokeService = invokeService;
    }
}
