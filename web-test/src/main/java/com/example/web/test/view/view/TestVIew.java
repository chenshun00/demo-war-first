package com.example.web.test.view.view;

import com.example.web.test.view.service.InnerService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * author chenshun00@gmail.com
 * 2020/3/5 10:12 上午
 */
@Controller
public class TestVIew {

    @Resource
    private InnerService innerService;

    @GetMapping("test")
    @ResponseBody
    public String test() {
        try {
            return innerService.handle();
        } catch (Throwable e) {
            return e.getMessage();
        }
    }

}

